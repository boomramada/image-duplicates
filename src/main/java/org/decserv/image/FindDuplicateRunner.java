package org.decserv.image;

import java.awt.EventQueue;

import javax.swing.UIManager;

import org.apache.log4j.BasicConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.decserv.image.config.SpringConfig;
import org.decserv.image.swing.MainWindow;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class FindDuplicateRunner {
	
	private static final Logger logger = LogManager.getLogger(FindDuplicateRunner.class);
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		logger.debug("Running gui application");
		BasicConfigurator.configure();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					@SuppressWarnings("resource")
					ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					MainWindow mainWindow = context.getBean(MainWindow.class);
					mainWindow.setVisible(true);

				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
		});
	}
}
