package org.decserv.image.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan(basePackages = { "org.decserv.image.config", "org.decserv.image.service", "org.decserv.image.swing" })
public class SpringConfig {
	@Autowired
	private Environment environment;

	/**
	 * Properties to support the 'default|production' mode of operation.
	 */
	@Configuration
	@Profile("default")
	@PropertySource("classpath:application-prod.properties")
	static class Default {
	}

}
