package org.decserv.image.service;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@Validated
@Configuration
public class ApplicationSettings {
	
	@NonNull
	@Value( "${resouce.path}" )
	private String resourcePath;
	
	
	@Value( "${validate.year:#{null}}" )
	private Integer validationYear;

	@Value( "${sub.path:#{null}}" )
	private String subPath;
	

	public String getSubPath() {
		return subPath;
	}


	public String getResourcePath() {
		return resourcePath;
	}


	public Integer getValidationYear() {
		return validationYear;
	}

}
