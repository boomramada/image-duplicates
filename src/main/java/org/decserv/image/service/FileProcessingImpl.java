package org.decserv.image.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

@Service
public class FileProcessingImpl implements IFileProcessing {
	private static final Logger logger = LogManager.getLogger(FileProcessingImpl.class);

	@Autowired
	ApplicationSettings applicationSettings;

	@Override
	public void writeFile(File path, String fileName, List<String> list) {
		if (list.size()>0) {
			logger.info("writing (items = (" + list.size() + ")) to file " + fileName);
		} else {
			logger.info("nothing to write to " + fileName);
			return;			
		}
		try {			
			
			File f = new File(path.getAbsolutePath() + "\\" + fileName);
			logger.info("writing to " + f.getAbsolutePath());
			FileOutputStream fos = new FileOutputStream(f);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			for (String string : list) {
				logger.info(string);	
				bw.write(string);
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * Generate a duplicate files based on the hashMap
	 * @param path
	 * @param fileName
	 * @param dulicateMap
	 */
	@Override
	public void writeFile(File path, String fileName, HashMap<String, List<ImageInfo>> duplicateMap) {
		if (!duplicateMap.isEmpty() ) {
			logger.info("writing (items = (" + duplicateMap.size() + ")) to file " + fileName);
		} else {
			logger.info("nothing to write to " + fileName);
			return;			
		}
		try {
			File f = new File(path.getAbsolutePath() + "\\" + fileName);
			logger.info("writing to " + f.getAbsolutePath());
			FileOutputStream fos = new FileOutputStream(f);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for (Map.Entry<String, List<ImageInfo>> entry : duplicateMap.entrySet()) {
				List<ImageInfo> imgList = entry.getValue();
				//only shows the duplicates
				if (imgList.size()>1) {
					logger.info(entry.getKey());
					bw.write(entry.getKey());
					bw.newLine();
				    for (ImageInfo imageInfo : entry.getValue()) {				    	
				    	logger.info(imageInfo.getPath());
				    	bw.write(imageInfo.getPath());
				    	bw.newLine();
					}
				}		    
			}
			bw.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	

	@Override
	public List<File> listf(File directory) {
		logger.info("processing path " + directory.getPath());
		List<File> resultList = new ArrayList<File>();
		try {
			// get all the files from a directory
			File[] fList = directory.listFiles();
			resultList.addAll(Arrays.asList(fList));
			for (File file : fList) {
				if (file.isFile()) {
					logger.debug(file.getAbsolutePath());
				} else if (file.isDirectory()) {
					resultList.addAll(listf(file));
				}
			}
		} catch (Exception e) {
			logger.error("Unable to get the file list for " + directory.getPath() + " " + e.getMessage());
		}
		logger.debug("file size for " + directory.getPath() + " " + resultList.size());
		return resultList;
	}

	@Override
	public List<ImageInfo> getImageList(File path, List<File> listOfFiles, Integer validateYear)  {
		List<ImageInfo> list = new ArrayList<ImageInfo>();
		List<String> errorList = new ArrayList<String>();
		for (File file : listOfFiles) {
			if (file.isFile()) {
				if (isValidExtention(file)) {
					try {
						ImageInfo info = getImageInfo(file);					
						if ((validateYear!=null) && ((info.getTakenYear() == -1) || (info.getTakenYear() != validateYear.intValue()))) {
							String st = "Invalid year (" + info.getTakenYear() + ") " + file.getPath();
							errorList.add(st);
							logger.info(st);
						} else {
							list.add(info);
						}
						
					} catch (IOException | ImageProcessingException e) {
						String st = "Invalid file format " + file.getPath() + ", ";
						errorList.add(st);
						logger.error(st + e.getMessage());
					} catch (Exception e) {
						String st = "Unable to process " + file.getPath() + ", ";
						errorList.add(st);
						logger.error(st + e.getMessage());
					}
				} else {
					String st = "Invalid file extention " + file.getPath() + ", ";
					errorList.add(st);
					logger.info(st);
				}
			} else {
				logger.debug(file.getName());
			}
		}
		writeFile(path,"error.txt", errorList);
		logger.info("end scanning.");

		return list;
	}
	
	@Override
	public ImageInfo getImageInfo(File file) throws ImageProcessingException, IOException {
		Metadata metadata = ImageMetadataReader.readMetadata(file);
		ImageInfo info = new ImageInfo(file.getName(), file.getPath(), getTimeTaken(metadata),String.valueOf(file.length()));
		return info;
		
	}

	@Override
	public boolean isValidExtention(File file) {
		List<String> invalidEx = Arrays.asList("db", "txt","png");
		if (invalidEx.contains(FilenameUtils.getExtension(file.getName()))) {
			return false;
		} else {
			return true;
		}
	}

	private static String getTimeTaken(Metadata metadata) {
		for (Directory directory : metadata.getDirectories()) {
			for (Tag tag : directory.getTags()) {
				if (tag.getDirectoryName().equals("Exif SubIFD") && tag.getTagName().equals("Date/Time Original")) {
					return tag.getDescription();
				}
			}
		}
		return "no-date";
	}

	public static void getMetadata(Metadata metadata) {
		System.out.println();
		System.out.println("-------------------------------------------------");
		System.out.print(' ');
		System.out.println("-------------------------------------------------");
		System.out.println();

		//
		// A Metadata object contains multiple Directory objects
		//
		for (Directory directory : metadata.getDirectories()) {
			//
			// Each Directory stores values in Tag objects
			//
			for (Tag tag : directory.getTags()) {
				System.out.println(tag);
			}

			//
			// Each Directory may also contain error messages
			//
			for (String error : directory.getErrors()) {
				System.err.println("ERROR: " + error);
			}
		}
	}

	@Override
	public List<ImageInfo> getDuplicates(final List<ImageInfo> fileList) {
		return getDuplicatesMap(fileList).values().stream().filter(duplicates -> duplicates.size() > 1)
				.flatMap(Collection::stream).collect(Collectors.toList());
	}

	/**
	 * Get list based on the checkFileSize attributes
	 * @param personList
	 * @param checkFileSize
	 * @return
	 */
	private static Map<String, List<ImageInfo>> getDuplicatesMap(List<ImageInfo> personList) {
		return personList.stream().collect(Collectors.groupingBy(ImageInfo::getUniqueAttributes));
	}

	@Override
	public void moveFile(File file) {
		moveFile(file, "moved");		
	}
	
	@Override
	public void moveFile(File file, String location) {
		logger.debug("source file " + file.getPath());
		logger.debug("location  " + location);
		Path fullpath = Paths.get(file.getPath());
		String directoryName = fullpath.getParent().toString() + "/" + location + "/";		
		File directory = new File(directoryName);		
		
	    if (! directory.exists()){
	        directory.mkdir();
	        // If you require it to make the entire directory path including parents,
	        // use directory.mkdirs(); here instead.
	    }
		
		String toMove = directoryName  + file.getName();
		logger.debug("destination " + toMove);	
		
		try {
			Files.move(Paths.get(file.getPath()), Paths.get(toMove));
		} catch (IOException e) {
			logger.error(e.getMessage());	
		}
		
	}

}
