package org.decserv.image.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import com.drew.imaging.ImageProcessingException;

public interface IFileProcessing {

	void writeFile(File path, String fileName, List<String> list);

	List<File> listf(File directory);

	List<ImageInfo> getImageList(File path, List<File> listOfFiles, Integer validateYear) throws ParseException;

	ImageInfo getImageInfo(File file) throws ImageProcessingException, IOException;

	boolean isValidExtention(File file);

	List<ImageInfo> getDuplicates(List<ImageInfo> personList);
	
	void moveFile(File file);

	/**
	 * Write list to file using the HashMap list of duplicates.
	 * @param path
	 * @param fileName
	 * @param dulicateMap
	 */
	void writeFile(File path, String fileName, HashMap<String, List<ImageInfo>> duplicateMap);

	void moveFile(File file, String location);
	

}