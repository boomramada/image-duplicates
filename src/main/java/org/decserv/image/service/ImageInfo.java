package org.decserv.image.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ImageInfo {
	
	private static final Logger logger = LogManager.getLogger(ImageInfo.class);

	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy:M:dd hh:mm:ss");

	private String name;
	private String path;
	private String dateTaken;
	private String fileSize;

	public ImageInfo(String name, String path, String dateTaken, String fileSize) {
		super();
		this.name = name;
		this.path = path;
		this.dateTaken = dateTaken;
		this.fileSize = fileSize;
	}
	private Date getTakenDate() {
		Date date;
		try {
			date = formatter.parse(dateTaken);
		} catch (Exception e) {
			logger.error("error getting year taken in " + this.getPath() + " "+  e.getMessage());
			// when there is no data taken set
			return null;
		}
		return date;
	}
	
	public int getTakenYear() {		
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime( getTakenDate());
		} catch (Exception e) {
			logger.error("error getting year taken in " + this.getPath() + " "+  e.getMessage());
			// when there is no data taken set
			return -1;
		}		
		return cal.get(Calendar.YEAR);
	}
	
	public String getTakenDateInString() {
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String stringDate = "no-date";
		if (getTakenDate()!=null) {
			stringDate = format1.format(getTakenDate());
		}
		return stringDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDateTaken() {
		return dateTaken;
	}

	public void setDateTaken(String dateTaken) {
		this.dateTaken = dateTaken;
	}
	

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * Used to match items
	 * 
	 * @return
	 */
	
	public String getUniqueAttributes(boolean withfileSize, boolean matchFileName) {
		StringBuilder sb = new StringBuilder();
		sb.append(dateTaken);
		
		if (withfileSize) {
			sb.append(fileSize);
		} 
		if (matchFileName) {
			sb.append(name);				
		}
		return sb.toString();
	}
	
	public String getUniqueAttributesWithoutFileSize() {
		return dateTaken;
	}

	public String getUniqueAttributesWithFileSize() {
		return dateTaken + "*" + fileSize;
	}
	
	public String getUniqueAttributes() {
		return dateTaken;
	}


}
