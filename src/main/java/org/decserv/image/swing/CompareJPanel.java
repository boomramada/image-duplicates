package org.decserv.image.swing;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

public class CompareJPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2552937839241663579L;

	public CompareJPanel(MainWindow mainWindow) {
		super();
		
		
		setBounds(100, 0, mainWindow.MAX_WINDOW_WIDTH-20, 100);
		setBorder(BorderFactory.createTitledBorder("Compare"));
		setLayout(null);
		// row three
		mainWindow.openTargetButton.setLayout(null);
		mainWindow.openTargetButton.setBounds(5, 15, 125, 20);
		add(mainWindow.openTargetButton);

		// row four
		mainWindow.targetTxtField = new JTextField();
		mainWindow.targetTxtField.setFont(mainWindow.fieldFont);
		mainWindow.targetTxtField
				.setText(mainWindow.sourcePath != null ? mainWindow.targetPath : "[not set - optional - use for when comapring two folders]");
		mainWindow.targetTxtField.setEditable(false);
		mainWindow.targetTxtField.setLayout(null);
		mainWindow.targetTxtField.setBounds(5, 40, 320, 20);
		add(mainWindow.targetTxtField);

		mainWindow.processButton.setLayout(null);
		mainWindow.processButton.setBounds(330, 70, 140, 20);
		mainWindow.processButton.setOpaque(true);
		add(mainWindow.processButton);
		
		mainWindow.progressBar=new JProgressBar();
		mainWindow.progressBar.setLayout(null);
		mainWindow.progressBar.setVisible(false);
		mainWindow.progressBar.setBounds(5, 75, 320, 15);
		add(mainWindow.progressBar);
		
	}
}
