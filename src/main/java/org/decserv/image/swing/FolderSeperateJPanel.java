package org.decserv.image.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FolderSeperateJPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton folderSeperationButton;
	private JTextField copyValidateTxtField;
	private JLabel copyValidateLabel;
	private JLabel infoJLabel;
	
	private static final Logger logger = LogManager.getLogger(FolderSeperateJPanel.class);

	public FolderSeperateJPanel(MainWindow mainWindow) {
		super();

		setBounds(100, 0, MainWindow.MAX_WINDOW_WIDTH - 20, 100);
		setBorder(BorderFactory.createTitledBorder("Seperate Folder"));
		setLayout(null);

		folderSeperationButton = new JButton("Seperate Folders");
		folderSeperationButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (mainWindow.sourcePath == null) {
					JOptionPane.showMessageDialog(null, "Folder has not set");
				} else {
					if (copyValidateTxtField.getText().equals("YES")) {
						// get the files in the folder	
						class MyWorker extends SwingWorker<Object, Object> {
						     protected String doInBackground() {	
						       try {	
						    	   folderSeperationButton.setEnabled(false);
						    	   mainWindow.progressBar.setVisible(true);
						    	   mainWindow.progressBar.setIndeterminate(true);
						    	   mainWindow.processFolderSeperation();
								} catch (Exception e1) {
									logger.error("Error on processing " + e1.getMessage());
								}
						       return "Done.";
						     }
						     protected void done() {
						    	 folderSeperationButton.setEnabled(true);
						    	 mainWindow.progressBar.setVisible(false);
						    	 mainWindow.windowLog.append("Folder seperation completed." + MainWindow.newline);
						     }
						  }
						  new MyWorker().execute();		
						
					} else {
						JOptionPane.showMessageDialog(null, "My Goodness, this is so concise");
					}
				}
				copyValidateTxtField.setText("NO");

			}
		});
		folderSeperationButton.setEnabled(true);
		folderSeperationButton.setLayout(null);
		folderSeperationButton.setBounds(330, 90, 140, 20);
		folderSeperationButton.setOpaque(true);
		add(folderSeperationButton);
		
		infoJLabel = new JLabel("<html>This will move the files within selected folders into subfolders based on the date the picture was taken.</html>");
		infoJLabel.setLayout(null);	
		infoJLabel.setBounds(5, 10, 400, 50);
		add(infoJLabel);

		copyValidateLabel = new JLabel("Type YES for verfy folder copy :");
		copyValidateLabel.setLayout(null);
		copyValidateLabel.setBounds(5, 70, 180, 20);
		add(copyValidateLabel);

		copyValidateTxtField = new JTextField();
		copyValidateTxtField.setFont(mainWindow.fieldFont);
		copyValidateTxtField.setText("NO");
		copyValidateTxtField.setEditable(true);
		copyValidateTxtField.setLayout(null);
		copyValidateTxtField.setBounds(200, 70, 80, 20);
		add(copyValidateTxtField);

	}

}
