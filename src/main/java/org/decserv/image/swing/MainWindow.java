package org.decserv.image.swing;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.Format;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.text.DefaultCaret;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.decserv.image.service.ApplicationSettings;
import org.decserv.image.service.IFileProcessing;
import org.decserv.image.service.ImageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jcabi.manifests.Manifests;

@Component
public class MainWindow extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2628047669548082084L;
	private static final Logger logger = LogManager.getLogger(MainWindow.class);

	public static final int MAX_WINDOW_HEIGHT =700; 
	public static final int MAX_WINDOW_WIDTH = 500;
	
	final Font fieldFont = new Font("SansSerif", Font.BOLD, 8);
	final Font logFont = new Font("Lucida Console", Font.PLAIN, 10);
	
	public static final String newline = "\n";

	JButton openSourceButton, openTargetButton, processButton;
	JTextArea windowLog;
	JFileChooser fileChooser;
	JFormattedTextField numPeriodsField;
	
	JScrollPane logScrollPane;
	String sourcePath = null, targetPath = null;
	Integer yearTaken;
	JTextField sourceTxtField, targetTxtField;
	JProgressBar progressBar;
	JCheckBox moveDups, matchFileName, checkFileSize;
	

	@Autowired
	ApplicationSettings applicationSettings;

	@Autowired
	IFileProcessing fileProcessing;

	@PostConstruct
	private void initialize() {
		setTitle("Image Duplicate Finder (" + getVersion() + ")");
		setResizable(false);
		windowLog = new JTextArea(3, 5);
		windowLog.setFont(logFont);
		windowLog.setLineWrap(true);
		windowLog.setWrapStyleWord(true);
		
		getContentPane().setLayout(
			    new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS)
			);

		// auto scroll
		DefaultCaret caret = (DefaultCaret) windowLog.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		logScrollPane = new JScrollPane(windowLog);
		logScrollPane.setPreferredSize(new Dimension(MAX_WINDOW_WIDTH, 180));
		logScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		// Create a file chooser
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("c:\\images"));
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		openSourceButton = new JButton("Set source path");
		openSourceButton.addActionListener(this);

		openTargetButton = new JButton("Set target path");
		openTargetButton.addActionListener(this);

		processButton = new JButton("Search");
		processButton.addActionListener(this);
		processButton.setEnabled(false);

		// Year field
		Format format = NumberFormat.getIntegerInstance();
		((NumberFormat) format).setGroupingUsed(false);
		
		numPeriodsField = new JFormattedTextField(format);
		
		numPeriodsField.addKeyListener(new KeyAdapter() {
		    public void keyTyped(KeyEvent e) { 
		        if (numPeriodsField.getText().length() >= 4 ) // limit textfield to 4 characters
		            e.consume(); 
		    }  
		});


		final JPanel searchDuplicateJPanel = new SearchDuplicateJPanel(this); 
		final JPanel compareJPanel = new CompareJPanel(this); 		
		final JPanel folderSeperateJPanel = new FolderSeperateJPanel(this);
				

		// Add the buttons and the log to this panel.
		add(searchDuplicateJPanel);
		add(compareJPanel);
		add(folderSeperateJPanel);
		add(logScrollPane);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Display the window.
		pack();

		// make the frame half the height and width
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height;
		int width = screenSize.width;
		logger.debug("window height  :" + height + " width :" + width);
		setSize(MAX_WINDOW_WIDTH,MAX_WINDOW_HEIGHT);

		// here's the part where i center the jframe on screen
		setLocationRelativeTo(null);
		setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == openSourceButton) {
			int returnVal = fileChooser.showOpenDialog(MainWindow.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				sourcePath = file.getPath();
				processButton.setText("Search Duplicate");
				processButton.setEnabled(true);
				sourceTxtField.setText(sourcePath != null ? sourcePath : "[not set]");
				printDefaultMessage();
			}
			windowLog.setCaretPosition(windowLog.getDocument().getLength());

		} else if (e.getSource() == openTargetButton) {
			int returnVal = fileChooser.showSaveDialog(MainWindow.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				targetPath = file.getPath();
				windowLog.append("Target Path: " + file.getPath() + "." + newline);
				processButton.setText("Compare");
				processButton.setEnabled(true);
				targetTxtField.setText(targetPath != null ? targetPath : "[not set]");
				windowLog.append("Duplicate files will move to " + targetPath + "\\move" + newline);

			} else {
				windowLog.append("Target path reset." + newline);
				processButton.setText("Search Duplicate");
				targetPath = null;
				targetTxtField.setText(targetPath != null ? targetPath : "[not set]");
			}
			windowLog.setCaretPosition(windowLog.getDocument().getLength());
		} else if (e.getSource() == processButton) {
			//clear window
			windowLog.setText("--------------------------------------------" + newline);	
			printDefaultMessage();
			logger.info("path :" + fileChooser.getSelectedFile());
			windowLog.append("Processing : " + fileChooser.getSelectedFile() + " .."+ newline);
			windowLog.setCaretPosition(windowLog.getDocument().getLength());
			
			class MyWorker extends SwingWorker<Object, Object> {
			     protected String doInBackground() {	
			       try {
						Integer year = null;
						try {
							year = Integer.valueOf(Integer.parseInt(numPeriodsField.getText()));
							 progressBar.setVisible(true);
						     progressBar.setIndeterminate(true);
						} catch (Exception e1) {
							windowLog.append("No year taken supplied for validation " + newline);
							JOptionPane.showMessageDialog (null, "No year taken supplied [yyyy]", "Error", JOptionPane.ERROR_MESSAGE);
							return "";
						}
						executeSearch(sourcePath, targetPath, year,checkFileSize.isSelected(),matchFileName.isSelected(),moveDups.isSelected());
					} catch (Exception e1) {
						logger.error("Error on processing " + e1.getMessage());
					}
			       return "Done.";
			     }

			     protected void done() {
			        progressBar.setVisible(false);
			     }
			  }
			  new MyWorker().execute();			
		}
	}
	

	
	private void printDefaultMessage() {
		windowLog.append("A list of duplicate/error (error.txt/duplicates.txt) may written to Source Path" + newline+newline);
		windowLog.append("Source Path: " + sourcePath + "." + newline);
		windowLog.append("Year taken: " + numPeriodsField.getText() + "." + newline);		
	}

	/**
	 * Perform search
	 * 
	 * @param sourcePath
	 * @param targetPath
	 * @param year
	 * @throws Exception
	 */
	private void executeSearch(String sourcePath, String targetPath, Integer year, boolean checkFileSize, boolean matchFileName, boolean moveFile) throws Exception {
		logger.info("sourcePath : " + sourcePath);
		logger.info("targetPath : " + targetPath);
		logger.info("validate year : " + year);

		File path = new File(sourcePath);
		List<ImageInfo> list =getListOfInfoForFolder();
	
		windowLog.append("Number of items to process : " + list.size() + newline);

		if (targetPath != null) {
			
			logger.info("processing folder compare..");

			File subPath = new File(targetPath);
			List<ImageInfo> subList = getImageInfoForfolder(subPath, null);
			for (ImageInfo sub : subList) {
				for (ImageInfo main : list) {
					if (main.getDateTaken().equals(sub.getDateTaken())) {
						logger.info("duplicate found " + sub.getPath());
						fileProcessing.moveFile(new File(sub.getPath()));
					}
				}
			}
		} else {
			// check for duplicate
			logger.info("finding duplicates..");
			try {
				checkDuplicate(path, list,checkFileSize,matchFileName, moveFile);
			} catch (Exception e) {
				if (e.getMessage().equals("element cannot be mapped to a null key")) {
					windowLog.append("ERROR : There is a file with no date taken. " + newline);
				} else {
					logger.error(e.getMessage());
				}
			}
		}
		windowLog.append("Search completed. Check the logs." + newline);
	}
	
	private List<ImageInfo> getListOfInfoForFolder() {
		File path = new File(sourcePath);

		List<ImageInfo> list = null;
		try {
			list = getImageInfoForfolder(path, yearTaken);
		} catch (Exception e) {
			logger.error("Error getting image info, " + e.getMessage());
		}
		return list;
	}

	/**
	 * Getting image information from files within the folder.
	 * 
	 * @param path
	 * @param valiationYear (Optional)
	 * @return
	 * @throws Exception
	 */
	private List<ImageInfo> getImageInfoForfolder(File path, Integer valiationYear) {
		List<File> listOfFiles = new ArrayList<File>();
		try {
			listOfFiles = fileProcessing.listf(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("List of file(s) :" + listOfFiles.size());
		
		List<ImageInfo> infoList = null;
		try {
			infoList = fileProcessing.getImageList(path, listOfFiles, valiationYear);
		} catch (Exception e) {
			logger.error("Error ocour while generating image list, " + e.getMessage());
		}
		if (infoList.size()==0) {
			logger.info("Unable to generate ImageInfo for " + path.getPath() );
		}
		
		return infoList;
	}
	
	public void processFolderSeperation() {
		windowLog.append("Processing : " + fileChooser.getSelectedFile() + " .."+ newline);
		List<ImageInfo> list = getListOfInfoForFolder();
		for (ImageInfo imageInfo : list) {
			windowLog.append("Moving : " + imageInfo.getPath() + " to " +  imageInfo.getTakenDateInString() + newline);
			fileProcessing.moveFile(new File(imageInfo.getPath()),imageInfo.getTakenDateInString());
		}
	}

	/**
	 * Look for file duplicates
	 * 
	 * @param path
	 * @param list
	 */
	private void checkDuplicate(File path, List<ImageInfo> list, boolean checkFileSize, boolean matchFileName,  boolean moveFile) {
		List<ImageInfo> duplicates = fileProcessing.getDuplicates(list);
		logger.info("Potential Duplicate found :" + duplicates.size());
		logger.info("check fileSize :" + checkFileSize);
		//having a gap between search results
		for (int i = 0; i < 5; i++) {
			windowLog.append("" + newline);			
		}
		windowLog.append("Potential Duplicate found :" + duplicates.size() + newline);
		List<String> dupList = new ArrayList<String>();

		for (ImageInfo imageInfo : duplicates) {
			dupList.add(imageInfo.getPath());
		}
		// sort the list
		Collections.sort(dupList);
		
		HashMap<String, List<ImageInfo>> hashMap = new HashMap<String, List<ImageInfo>>();
		for (ImageInfo img : list) {		
			if (!hashMap.containsKey(img.getUniqueAttributes(checkFileSize,matchFileName))) {
			    List<ImageInfo> imgList = new ArrayList<ImageInfo>();
			    imgList.add(img);
			    hashMap.put(img.getUniqueAttributes(checkFileSize,matchFileName), imgList);
			} else {
			    hashMap.get(img.getUniqueAttributes(checkFileSize,matchFileName)).add(img);
			}
		}
		fileProcessing.writeFile(path, "duplicates.txt", hashMap);	
		displayDuplicateToWindow(hashMap);
		if (moveFile) {
			moveFiles(hashMap);
		}
	}
	
	/**
	 * Display the duplicate data to screen
	 * @param duplicates
	 */
	private void displayDuplicateToWindow(HashMap<String, List<ImageInfo>> duplicates) {
		for (Map.Entry<String, List<ImageInfo>> entry : duplicates.entrySet()) {
			List<ImageInfo> imgList = entry.getValue();
			//only shows the duplicates
			if (imgList.size()>1) {
				logger.info(entry.getKey());
				windowLog.append(entry.getKey() + newline);
			    for (ImageInfo imageInfo : entry.getValue()) {				    	
			    	logger.info(imageInfo.getPath());
			    	windowLog.append(imageInfo.getPath() + newline);
				}
			}		    
		}
	}
	
	/**
	 * Move files
	 * @param duplicates
	 */
	private void moveFiles(HashMap<String, List<ImageInfo>> duplicates) {
		for (Map.Entry<String, List<ImageInfo>> entry : duplicates.entrySet()) {
			List<ImageInfo> imgList = entry.getValue();
			//only shows the duplicates
			if (imgList.size()>1) {
				windowLog.append(entry.getKey() + newline);
				//keep one, move remaining
				for (int i = 1; i < imgList.size(); i++) {
					//move files
					ImageInfo img = imgList.get(i);
					fileProcessing.moveFile(new File(img.getPath()));
					logger.info("moving file " + img.getPath());
				}
			}		    
		}
	}
	

	/**
	 * Getting the version from MAINFEST
	 * 
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	private String getVersion() {
		String version = "1.0";
		try {
			version+=Manifests.read("SCM-Revision");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return version;
	}
}
