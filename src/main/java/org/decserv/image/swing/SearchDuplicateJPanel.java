package org.decserv.image.swing;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchDuplicateJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9216748481683197221L;
	private JLabel numPeriodsLabel;

	public SearchDuplicateJPanel(MainWindow mainWindow) {

		// widith/height
		setLayout(null);
		setBounds(0, 0, MainWindow.MAX_WINDOW_WIDTH - 20, 300);
		setBorder(BorderFactory.createTitledBorder("Find Duplicate"));

		// row one
		mainWindow.openSourceButton.setBounds(5, 15, 125, 20);
		add(mainWindow.openSourceButton);

		// check for size of the file when comparing
		mainWindow.checkFileSize = new JCheckBox("Match file size");
		mainWindow.checkFileSize.setBounds(325, 40, 120, 20);
		add(mainWindow.checkFileSize);

		// check file name

		mainWindow.matchFileName = new JCheckBox("Match file name");
		mainWindow.matchFileName.setBounds(325, 60, 120, 20);
		add(mainWindow.matchFileName);

		// move files to temp folder
		mainWindow.moveDups = new JCheckBox("Move Duplicates");
		mainWindow.moveDups.setBounds(325, 80, 120, 20);
		mainWindow.moveDups.setBackground(Color.RED);
		add(mainWindow.moveDups);

		// row two
		numPeriodsLabel = new JLabel("Year Taken (yyyy) :");
		numPeriodsLabel.setLayout(null);
		numPeriodsLabel.setBounds(300, 15, 120, 20);
		add(numPeriodsLabel);

		mainWindow.numPeriodsField.setLayout(null);
		mainWindow.numPeriodsField.setBounds(410, 15, 60, 20);
		add(mainWindow.numPeriodsField);

		mainWindow.sourceTxtField = new JTextField();
		mainWindow.sourceTxtField.setFont(mainWindow.fieldFont);
		mainWindow.sourceTxtField
				.setText(mainWindow.sourcePath != null ? mainWindow.sourcePath : "[not set - use to find duplicates within the folder]");
		mainWindow.sourceTxtField.setEditable(false);
		mainWindow.sourceTxtField.setLayout(null);
		mainWindow.sourceTxtField.setBounds(5, 40, 320, 20);
		add(mainWindow.sourceTxtField);
	}

}
