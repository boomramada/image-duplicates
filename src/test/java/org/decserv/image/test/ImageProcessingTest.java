package org.decserv.image.test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.decserv.image.service.IFileProcessing;
import org.decserv.image.service.ImageInfo;
import org.decserv.image.test.conf.SpringConfigTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.drew.imaging.ImageProcessingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SpringConfigTest.class })
@SpringBootTest
public class ImageProcessingTest {

	@BeforeClass
	public static void setupHeadlessMode() {
		System.setProperty("java.awt.headless", "false");
	}

	@Autowired
	IFileProcessing fileProcessing;

	@Test
	public void testGetImageInfo() throws ImageProcessingException, IOException {
		String path = "src/test/resources/DSC02869.JPG";
		File file = new File(path);

		ImageInfo image = fileProcessing.getImageInfo(file);

		assertEquals(image.getTakenYear(), 2011);
		assertEquals(image.getName(), "DSC02869.JPG");
		assertEquals(image.getDateTaken(), "2011:01:30 14:09:25");
		assertEquals(image.getPath().replace("/", "").replace("\\", ""), "srctestresourcesDSC02869.JPG");

	}

	@Test
	public void testListf() {
		// including the 2 none image files
		assertTrue(4 < getTestFiles().size());
	}

	@Test
	public void testGetImageList() throws ParseException {
		String path = "src/test/resources";
		File file = new File(path);

		List<ImageInfo> list = fileProcessing.getImageList(file, getTestFiles(), 2011);
		assertEquals(4, list.size());
	}

	@Test
	public void testGetDuplicates() throws ParseException {
		String path = "src/test/resources";
		File file = new File(path);

		List<ImageInfo> list = fileProcessing.getImageList(file, getTestFiles(), 2011);
		assertEquals(fileProcessing.getDuplicates(list).size(), 2);
	}

	@Test
	public void testTakenYear() throws ParseException {
		String path = "src/test/resources";
		File file = new File(path);
		List<ImageInfo> list = fileProcessing.getImageList(file, getTestFiles(), 2011);

		ImageInfo info = list.get(0);
		assertEquals(info.getTakenYear(), 2011);
	}

	private List<File> getTestFiles() {
		String path = "src/test/resources";
		File file = new File(path);

		return fileProcessing.listf(file);
	}

}
