package org.decserv.image.test.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;


/**
 * Package org.decserv.image.test.config to separate the GUI against the rest of the application,
 * else UNIT test try to run the GUI application during the test.
 * @author boomramada
 *
 */
@Configuration
@ComponentScan(basePackages = { "org.decserv.image.test.config", "org.decserv.image.service"})
public class SpringConfigTest {
	
	/**
	 * Properties to support the 'default' mode of operation for test only.
	 */
	@Configuration
	@Profile("default")
	@PropertySource("classpath:application-test.properties")
	static class Default {
	}
}
